const apiKey = "YOUR_OPENWEATHERMAP_API_KEY";

const cities = {
  newYork: {
    elementId: "newYorkWeather",
    apiURL: `https://api.openweathermap.org/data/2.5/forecast?q=New%20York,us&units=metric&appid=${apiKey}`,
  },
  london: {
    elementId: "londonWeather",
    apiURL: `https://api.openweathermap.org/data/2.5/forecast?q=London,uk&units=metric&appid=${apiKey}`,
  },
};

for (let city in cities) {
  fetch(cities[city].apiURL)
    .then((response) => response.json())
    .then((data) => {
      const weatherElement = document.getElementById(cities[city].elementId);
      weatherElement.innerHTML = `
        <h2 class="text-2xl mb-4">${data.city.name}</h2>
        <p class="mb-2"><strong>Current Temperature:</strong> ${data.list[0].main.temp}°C</p>
        <p class="mb-2"><strong>Weather:</strong> ${data.list[0].weather[0].description}</p>
        <h3 class="text-xl mb-2">7-Day Forecast:</h3>
        <ul>
          ${data.list
            .slice(0, 7)
            .map(
              (day) =>
                `<li>${new Date(day.dt * 1000).toLocaleDateString()}: ${
                  day.main.temp
                }°C, ${day.weather[0].description}</li>`
            )
            .join("")}
        </ul>
      `;
    })
    .catch((error) => console.error("Error:", error));
}
